package articulos;
import java.awt.EventQueue; 
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Formulario extends JFrame{
      private JPanel contentPane;
      private JTextField tf1;
      private JTextField tf2;
      private JLabel labelResultado;
      private JButton btnConsultaPorCdigo;
      private JLabel lblIngreseCdigoDe;
      private JTextField tf3;
      
      /* Lanch the application */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable(){
            public void run(){
                try{
                    Formulario frame = new Formulario();
                    frame.setVisible(true);
                }catch( Exception e ){
                    e.printStackTrace();
                }
            }
        });
    }
    
    /* Create the frame*/
    public Formulario(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds( 100, 100, 606, 405 );
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5,5,5,5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JLabel lblDescripcinDelArticulo = new JLabel("Descripcion del articulo:");
        lblDescripcinDelArticulo.setBounds(23, 38, 193, 14);
        contentPane.add(lblDescripcinDelArticulo);
        
        tf1 = new JTextField();
        tf1.setBounds(247, 35, 193, 20);
        contentPane.add(tf1);
        tf1.setColumns(10);
        
        JLabel lblPrecio = new JLabel("Precio");
        lblPrecio.setBounds(23, 74, 95, 14);
        contentPane.add(lblPrecio);
        
        tf2 = new JTextField();
        tf2.setBounds(247, 71, 107, 20);
        contentPane.add(tf2);
        tf2.setColumns(10);
        
    /*Boton Alta ->Agregará filas a la tabla articulos*/
        JButton btnAlta = new JButton("Alta");
        
        btnAlta.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0){
                /*En el actionPerformed procedemos a limpiar la albel que puede tener un mensaje de ejecuciones anteriores*/
                labelResultado.setText("");
                
                /*Toda clase orientada al acceso de BD genera excepciones y deben ser capturadas obligatoriamente*/
                try{
                    /*Se debe crear objeto Connection, la clase DriverManager tiene un metodo que retorna objeto de la clase Connection(getConnection)*/
                    Connection
                            /*A getConnectio se le pasa tres Strings*/
                            conexion=DriverManager.getConnection("jdbc:mysql://localhost/basededatos", "root", "");//1ºNombre de BD, 2ºNombre de usuraio, 3ºLa clave del usuario "root"
                    Statement comando = conexion.createStatement();
                    /*La clase Statement tiene un método llamado executeUpdate que le pasamos el comando SQL insert para agregar una fila a la tabla articulos*/
                    comando.executeUpdate("INSERT articulos(descripcion,precio) VALUES ('"+ tf1.getText() + "','" + tf2.getText()+ "')");
                    
                    /*Luego de solicitar el comando a MySQL procedemos al metodo close de la clase Connection*/
                    conexion.close();
                    labelResultado.setText("Se registraron los datos");
                    tf1.setText("");
                    tf2.setText("");
                }catch( SQLException ex){
                    /*Si hay algun error veremo el tipo de error en el tirulo del JFrame*/
                    setTitle(ex.toString());
                }
            }
        });
        btnAlta.setBounds(247, 118, 89, 23);
        contentPane.add(btnAlta);
        
        labelResultado = new JLabel("Resultado");
        labelResultado.setBounds(361, 122, 229, 14);
        contentPane.add(labelResultado);
        
        
    /*Boton Consulta-> Modo de consultar datos al presionar Botón*/
        btnConsultaPorCdigo = new JButton("Consulta por codigo");
        
        btnConsultaPorCdigo.addActionListener(new ActionListener(){ 
            public void actionPerformed(ActionEvent arg0){
                labelResultado.setText("");
                tf1.setText("");
                tf2.setText("");
                
                try{
                    /*De forma similar a btnAlta procedemos a crear un objeto Connection y otro objeto de clase Statement*/
                    Connection
                        conexion = DriverManager.getConnection("jdbc:mysql://localhost/basededatos", "root", "");
                    Statement comando = conexion.createStatement();
                    
                    /*Definimos una variable "registro" de la clase ResultSet y llamamos al metodo executeQuery dela claseStatement recientemente creado*/
                    ResultSet registro = comando.executeQuery("SELECT descripcion, precio FROM articulos WHERE codigo=" + tf3.getText());
                    /*ResulSet se puede imaginar como una tabla con todos los datos recuperados del comando SQL SELECT*/
                    
                    
                    /*Para acceder al registro devuelto debemos llamar al método next()
                    Si retorna:
                    true: recupero una fula de la tabla
                    false: no habría un articulo con el código ingresado*/
                    if( registro.next() == true){
                        tf1.setText(registro.getString("descripcion"));
                        tf2.setText(registro.getString("precio"));
                    } else {
                        labelResultado.setText("No existe un artículo condicho código");
                    }
                    conexion.close();
                }catch(SQLException ex){
                    setTitle(ex.toString());
                }
            }
        });
        btnConsultaPorCdigo.setBounds(23, 212, 177, 23);
        contentPane.add(btnConsultaPorCdigo);
        
        lblIngreseCdigoDe = new JLabel("Ingrese código de articulo a consultar");
        lblIngreseCdigoDe.setBounds(10, 179, 243, 14);
        contentPane.add(lblIngreseCdigoDe);
        tf3 = new JTextField();
        tf3.setBounds(247, 176, 86, 20);
        contentPane.add(tf3);
        tf3.setColumns(10);
        
        
    /*Boton Borrar-> Elimina elemento de la tabla articulos en la BD*/
        JButton btnBorrar = new JButton("Borrar");
        btnBorrar.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent arg0){
               labelResultado.setText("");
               try{
                   Connection
                           conexion=DriverManager.getConnection("jdbc:mysql://localhost/basededatos","root","");
                   /*creamos objeto de clase Statement*/
                   Statement comando = conexion.createStatement();
                   /**/
                   int cantidad = comando.executeUpdate("DELETE FROM articulos WHERE codigo = " + tf3.getText());
                   if ( cantidad == 1 ){
                       tf1.setText("");
                       tf2.setText("");
                       labelResultado.setText("Se borro el artículo con dicho código");
                   } else {
                       labelResultado.setText("No existe un articulo con dicho código");
                   }
                   conexion.close();
               }catch( SQLException ex ){
                   setTitle( ex.toString() );
               }
           } 
        });
        
        btnBorrar.setBounds( 361, 212, 89, 23);
        contentPane.add(btnBorrar);
        
        JButton btnModificar = new JButton("Modificar");
        btnModificar.addActionListener(new ActionListener(){
            public void actionPerformed( ActionEvent e ){
                labelResultado.setText("");
                try{
                    Connection
                            conexion =DriverManager.getConnection("jdbc:mysql://localhost/basededatos","root","");
                    Statement comando = conexion.createStatement();
                    int cantidad = comando.executeUpdate("UPDATE articulos SET descripcion='" + tf1.getText() + "'," + " precio= " + tf2.getText() + " WHERE codigo= " + tf3.getText());
                    if( cantidad==1 ){
                        labelResultado.setText("Se modificó la descripción y el precio del artículo con dicho código");
                    } else {
                        labelResultado.setText("No existe un artículo con dicho código");
                    }
                    conexion.close();
                }catch( SQLException ex ){
                    setTitle( ex.toString() );
                }
            }
        });
        btnModificar.setBounds(247, 212, 89, 23);
        contentPane.add(btnModificar);
        
        /*Llamamos por unica vez el metodo cargarDriver*/
        cargarDriver();
    }
    
/*Cargamos en memoria el Driver mediante el método cargarDriver que es 
    llamado luego desde el constructor de la clase*/
    private void cargarDriver(){
        try{
            /*clase Class con metodo estatico fsorName donde
                    se le pasa el nombre de la clase a importar*/
            Class.forName("com.mysql.jdbc.Driver");
        }catch(Exception ex){
            setTitle(ex.toString());
            System.out.println( ex );
        }
    }
}
